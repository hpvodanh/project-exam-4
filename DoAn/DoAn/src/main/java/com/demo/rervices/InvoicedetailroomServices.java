package com.demo.rervices;


import com.demo.models.Invoicedetailroom;
import com.demo.models.InvoicedetailroomId;

public interface InvoicedetailroomServices {

	public Iterable<Invoicedetailroom> findAll();
	public Invoicedetailroom find(InvoicedetailroomId id);
	public Invoicedetailroom save(Invoicedetailroom invoicedetailroom);
	public void delete(InvoicedetailroomId id);

}
