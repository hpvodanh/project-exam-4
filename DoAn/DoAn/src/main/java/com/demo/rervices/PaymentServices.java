package com.demo.rervices;


import com.demo.models.Payment;

public interface PaymentServices {

	public Iterable<Payment> findAll();
	public Payment find(int id);
	public Payment save(Payment Payment);
	public void delete(int id);
}
