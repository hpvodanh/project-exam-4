package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Rooms;
import com.demo.repositories.RoomsResponsitory;

@Service("roomsServices")
public class RoomsServicesImpl implements RoomsServices{

	
	@Autowired
	private RoomsResponsitory roomsResponsitory;

	@Override
	public Iterable<Rooms> findAll() {
		return roomsResponsitory.findAll();
	}

	@Override
	public Rooms find(int id) {
		return roomsResponsitory.findById(id).get();
	}

	@Override
	public Rooms save(Rooms rooms) {
		return roomsResponsitory.save(rooms);
	}

	@Override
	public void delete(int id) {
		roomsResponsitory.deleteById(id);
		
	}
}
