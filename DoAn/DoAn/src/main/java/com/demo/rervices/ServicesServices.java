package com.demo.rervices;

import com.demo.models.Services;

public interface ServicesServices {
	public Iterable<Services> findAll();
	public Services find(int id);
	public Services save(Services services);
	public void delete(int id);
}
