package com.demo.models;
// Generated Dec 16, 2020, 8:55:54 AM by Hibernate Tools 5.2.12.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Invoidetailservice generated by hbm2java
 */
@Entity
@Table(name = "invoidetailservice", catalog = "homiehotels")
public class Invoidetailservice implements java.io.Serializable {

	private InvoidetailserviceId id;
	private Invoices invoices;
	private Services services;
	private Double totalService;

	public Invoidetailservice() {
	}

	public Invoidetailservice(InvoidetailserviceId id, Invoices invoices, Services services) {
		this.id = id;
		this.invoices = invoices;
		this.services = services;
	}

	public Invoidetailservice(InvoidetailserviceId id, Invoices invoices, Services services, Double totalService) {
		this.id = id;
		this.invoices = invoices;
		this.services = services;
		this.totalService = totalService;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "invoiceId", column = @Column(name = "invoiceId", nullable = false)),
			@AttributeOverride(name = "serviceId", column = @Column(name = "serviceId", nullable = false)) })
	public InvoidetailserviceId getId() {
		return this.id;
	}

	public void setId(InvoidetailserviceId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoiceId", nullable = false, insertable = false, updatable = false)
	public Invoices getInvoices() {
		return this.invoices;
	}

	public void setInvoices(Invoices invoices) {
		this.invoices = invoices;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "serviceId", nullable = false, insertable = false, updatable = false)
	public Services getServices() {
		return this.services;
	}

	public void setServices(Services services) {
		this.services = services;
	}

	@Column(name = "totalService", precision = 22, scale = 0)
	public Double getTotalService() {
		return this.totalService;
	}

	public void setTotalService(Double totalService) {
		this.totalService = totalService;
	}

}
