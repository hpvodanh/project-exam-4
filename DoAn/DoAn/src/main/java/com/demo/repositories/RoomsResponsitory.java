package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.demo.models.Payment;
import com.demo.models.Rooms;

@Repository("RoomsResponsitory")
public interface RoomsResponsitory extends CrudRepository<Rooms, Integer>{

}
