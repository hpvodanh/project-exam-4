package com.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.Role;

@Repository("RoleRepository")
public interface RoleRepository extends CrudRepository<Role, Integer>{

	@Query("from Role")
	List<Role> listRole();
}
