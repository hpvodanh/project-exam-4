package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.Roomtypes;


@Repository("RoomTypesRepository")
public interface RoomTypesRepository extends CrudRepository<Roomtypes, Integer>{

}
