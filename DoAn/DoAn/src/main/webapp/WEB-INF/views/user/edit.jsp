<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

	<h3>Edit</h3>
	<s:form method="POST" modelAttribute="user"
		action="${pageContext.request.contextPath }/user/edit">
		<table>
			<tr>
				<td>Username</td>
				<td><s:input path="username" disabled="true"/></td>
			</tr>
			<tr>
				<td>Roles</td>
				<td>
				<s:checkbox path="roles" value="8"/> ADMIN
				<br>
				<s:checkbox path="roles" value="9"/> USER
				</td>
			</tr>
			<tr>
				<td>FullName</td>
				<td><s:input path="fullName" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" value="Save"/>
					<s:hidden path="id"></s:hidden> 
					<s:hidden path="username"></s:hidden> 
					<s:hidden path="password"></s:hidden> 
				</td>
			</tr>
		</table>
	</s:form>
</body>
</html>